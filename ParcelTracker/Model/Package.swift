//
//  Package.swift
//  ParcelTracker
//
//  Created by Dean Mollica on 16/6/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

struct Package: Codable {
    var name: String
    var address: String
    var status: PackageStatus
    var lastUpdated: Date
    
    var trackingNumber: String?
    var deliveredDate: Date?
    var notes: String?
    
    static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.timeStyle = .short
        return formatter
    }()
    
    static let DocumentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectoryURL.appendingPathComponent("packages").appendingPathExtension("plist")
    
    static func loadPackages() -> [Package]? {
        guard let codedPackages = try? Data(contentsOf: ArchiveURL) else { return nil }
        
        let propertyListDecoder = PropertyListDecoder()
        return try? propertyListDecoder.decode(Array<Package>.self, from: codedPackages)
    }
    
    static func savePackages(_ packages: [Package]) {
        let propertyListEncoder = PropertyListEncoder()
        let codedPackages = try? propertyListEncoder.encode(packages)
        try? codedPackages?.write(to: ArchiveURL, options: .noFileProtection)
    }
}
