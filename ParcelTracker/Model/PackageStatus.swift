//
//  PackageStatus.swift
//  ParcelTracker
//
//  Created by Dean Mollica on 16/6/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

enum PackageStatus: Int, Comparable, Codable {
    case packing = 0, awaitingPickup = 1, inTransit = 2, delivered = 3
    
    var image: UIImage {
        switch self {
        case .packing:
            return UIImage(named: "Packing")!
        case .awaitingPickup:
            return UIImage(named: "Pickup")!
        case .inTransit:
            return UIImage(named: "Transit")!
        case .delivered:
            return UIImage(named: "Delivered")!
        }
    }
    
    var name: String {
        switch self {
        case .packing:
            return "Packing Items"
        case .awaitingPickup:
            return "Awaiting Pickup"
        case .inTransit:
            return "In Transit"
        case .delivered:
            return "Delivered"
        }
    }
    
    static func < (lhs: PackageStatus, rhs: PackageStatus) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}
