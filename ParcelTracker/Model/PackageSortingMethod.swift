//
//  PackageSortingMethod.swift
//  ParcelTracker
//
//  Created by Dean Mollica on 19/6/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import Foundation

enum PackageSortType: Int, Codable {
    case name = 0, lastUpdate = 1, status = 2
}

struct PackageSortingMethod: Codable {
    var sortType: PackageSortType
    var ascending: Bool
    
    static let DocumentsDirectoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectoryURL.appendingPathComponent("packageSortingMethod").appendingPathExtension("plist")
    
    static func loadPackageSortingMethod() -> PackageSortingMethod? {
        guard let codedPackageSortingMethod = try? Data(contentsOf: ArchiveURL) else { return nil }
        
        let propertyListDecoder = PropertyListDecoder()
        return try? propertyListDecoder.decode(PackageSortingMethod.self, from: codedPackageSortingMethod)
    }
    
    static func savePackageSortingMethod(_ packageSortingMethod: PackageSortingMethod) {
        let propertyListEncoder = PropertyListEncoder()
        let codedPackageSortingMethod = try? propertyListEncoder.encode(packageSortingMethod)
        try? codedPackageSortingMethod?.write(to: ArchiveURL, options: .noFileProtection)
    }
}
