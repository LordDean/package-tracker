//
//  PackageDetailTableViewController.swift
//  ParcelTracker
//
//  Created by Dean Mollica on 16/6/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

class PackageDetailTableViewController: UITableViewController {
    @IBOutlet weak var saveBarButton: UIBarButtonItem!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var statusPicker: UISegmentedControl!
    @IBOutlet weak var lastUpdatedDateLabel: UILabel!
    @IBOutlet weak var lastUpdatedDatePicker: UIDatePicker!
    @IBOutlet weak var lastUpdatedNowButton: UIButton!
    @IBOutlet weak var trackingNumberTextField: UITextField!
    @IBOutlet weak var deliveredDateLabel: UILabel!
    @IBOutlet weak var deliveredDatePicker: UIDatePicker!
    @IBOutlet weak var deliveredNowButton: UIButton!
    @IBOutlet weak var notesTextView: UITextView!
    
    var isEditingPackage = false
    var isLastUpdatedDatePickerHidden = true {
        didSet {
            lastUpdatedDatePicker.isUserInteractionEnabled = !isLastUpdatedDatePickerHidden
            lastUpdatedNowButton.isUserInteractionEnabled = !isLastUpdatedDatePickerHidden
        }
    }
    var isDeliveryDatePickerHidden = true {
        didSet {
            deliveredDatePicker.isUserInteractionEnabled = !isDeliveryDatePickerHidden
            deliveredNowButton.isUserInteractionEnabled = !isDeliveryDatePickerHidden
        }
    }
    var isDelivered = false
    
    let lastUpdatedDatePickerIndexPath = IndexPath(row: 3, section: 0)
    let deliveryDatePickerIndexPath = IndexPath(row: 1, section: 1)
    let notesTextFieldIndexPath = IndexPath(row: 0, section: 2)
    
    var package: Package?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let package = package {
            self.title = "Edit Package"
            isEditingPackage = true
            nameTextField.text = package.name
            addressTextField.text = package.address
            statusPicker.selectedSegmentIndex = package.status.rawValue
            lastUpdatedDatePicker.date = package.lastUpdated
            trackingNumberTextField.text = package.trackingNumber ?? ""
            deliveredDatePicker.date = package.deliveredDate ?? Date()
            notesTextView.text = package.notes ?? ""
        } else {
            self.title = "Add Package"
        }
        updateDateLabels()
        updateSaveButton()
    }
    
    func updateCells() {
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func updateDateLabels() {
        let lastUpdatedDate = Package.dateFormatter.string(from: lastUpdatedDatePicker.date)
        let deliveredDate = Package.dateFormatter.string(from: deliveredDatePicker.date)
        
        lastUpdatedDateLabel.text = lastUpdatedDate
        deliveredDateLabel.text = deliveredDate
    }
    
    func updateSaveButton() {
        let name = nameTextField.text ?? ""
        let address = addressTextField.text ?? ""
        
        saveBarButton.isEnabled = !name.isEmpty && !address.isEmpty
    }
    
    
    @IBAction func datePickerChanged(_ sender: UIDatePicker) {
        updateDateLabels()
    }
    
    @IBAction func textFieldReturnPressed(_ sender: UITextField) {
        sender.resignFirstResponder()
    }
    
    @IBAction func textFieldChanged(_ sender: UITextField) {
        updateSaveButton()
    }
    
    @IBAction func segmentedControlChanged(_ sender: UISegmentedControl) {
        isDelivered = sender.selectedSegmentIndex == PackageStatus.delivered.rawValue
        updateCells()
    }
    
    @IBAction func lastUpdatedNowButtonTapped() {
        lastUpdatedDatePicker.date = Date()
        updateDateLabels()
    }
    
    @IBAction func deliveredNowButtonTapped() {
        deliveredDatePicker.date = Date()
        updateDateLabels()
    }
    
    @IBAction func deleteButtonTapped() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        let deleteAction = UIAlertAction(title: "Delete", style: .destructive) { (_) in
            self.performSegue(withIdentifier: "DeletePackage", sender: self)
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(deleteAction)
        present(alertController, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "SavePackage" else { return }
        guard let name = nameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines),
            let address = addressTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {
            fatalError("Save button active with empty Name or Address")
        }
        
        let status = PackageStatus(rawValue: statusPicker.selectedSegmentIndex)!
        let lastUpdate = lastUpdatedDatePicker.date
        let trackingNumber = trackingNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let deliveredDate = status == .delivered ? deliveredDatePicker.date : nil
        let notes = notesTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        package = Package(name: name, address: address, status: status, lastUpdated: lastUpdate, trackingNumber: trackingNumber, deliveredDate: deliveredDate, notes: notes)
    }

}


extension PackageDetailTableViewController {
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let hiddenCellHeight = CGFloat(0)
        let normalCellHeight = CGFloat(44)
        let notesCellHeight = CGFloat(200)
        let largeCellHeight = CGFloat(233)
        
        switch indexPath {
        case lastUpdatedDatePickerIndexPath:
            return isLastUpdatedDatePickerHidden ? normalCellHeight : largeCellHeight
        case deliveryDatePickerIndexPath:
            if statusPicker.selectedSegmentIndex != PackageStatus.delivered.rawValue {
                return hiddenCellHeight
            } else {
                return isDeliveryDatePickerHidden ? normalCellHeight : largeCellHeight
            }
        case notesTextFieldIndexPath:
            return notesCellHeight
        default:
            return normalCellHeight
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath {
        case lastUpdatedDatePickerIndexPath:
            isLastUpdatedDatePickerHidden = !isLastUpdatedDatePickerHidden
            lastUpdatedDateLabel.textColor = isLastUpdatedDatePickerHidden ? .black : tableView.tintColor
        case deliveryDatePickerIndexPath:
            isDeliveryDatePickerHidden = !isDeliveryDatePickerHidden
            deliveredDateLabel.textColor = isDeliveryDatePickerHidden ? .black : tableView.tintColor
        default:
            break
        }
        updateCells()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return isEditingPackage ? 4 : 3
    }
}
