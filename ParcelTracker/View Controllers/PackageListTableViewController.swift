//
//  PackageListTableViewController.swift
//  ParcelTracker
//
//  Created by Dean Mollica on 16/6/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

class PackageListTableViewController: UITableViewController {
    var packages: [Package] = [] {
        didSet {
            Package.savePackages(packages)
        }
    }
    
    var packageSortingMethod = PackageSortingMethod(sortType: .lastUpdate, ascending: false) {
        didSet {
            PackageSortingMethod.savePackageSortingMethod(packageSortingMethod)
            sortPackages()
        }
    }
    
    let daysToKeepDeliveredPackages = 7
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let packages = Package.loadPackages() {
            self.packages = packages
        }
        
        if let sortingMethod = PackageSortingMethod.loadPackageSortingMethod() {
            self.packageSortingMethod = sortingMethod
        }
        sortPackages()
        removeOldDeliveries(numberOfDaysOld: daysToKeepDeliveredPackages)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        removeOldDeliveries(numberOfDaysOld: daysToKeepDeliveredPackages)
    }
    
    func sortPackages() {
        switch packageSortingMethod.sortType {
        case .name:
            packages.sort {
                if packageSortingMethod.ascending {
                    if $0.name == $1.name {
                        return $0.lastUpdated < $1.lastUpdated
                    } else {
                        return $0.name < $1.name
                    }
                } else {
                    if $0.name == $1.name {
                        return $0.lastUpdated > $1.lastUpdated
                    } else {
                        return $0.name > $1.name
                    }
                }
            }
        case .lastUpdate:
            packages.sort {
                if packageSortingMethod.ascending {
                    return $0.lastUpdated < $1.lastUpdated
                } else {
                    return $0.lastUpdated > $1.lastUpdated
                }
            }
        case .status:
            packages.sort {
                if packageSortingMethod.ascending {
                    if $0.status == $1.status {
                        return $0.lastUpdated < $1.lastUpdated
                    } else {
                        return $0.status < $1.status
                    }
                } else {
                    if $0.status == $1.status {
                        return $0.lastUpdated > $1.lastUpdated
                    } else {
                        return $0.status > $1.status
                    }
                }
            }
        }
        tableView.reloadData()
    }
    
    
    @IBAction func sortButtonTapped(_ sender: UIBarButtonItem) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        var nameSortName = "By Name ▲"
        var lastUpdatedSortName = "By Last Updated ▲"
        var statusSortName = "By Delivery Status ▲"
        
        switch packageSortingMethod.sortType {
        case .name:
            if packageSortingMethod.ascending { nameSortName = "By Name ▼" }
        case .lastUpdate:
            if packageSortingMethod.ascending { lastUpdatedSortName = "By Last Updated ▼" }
        case .status:
            if packageSortingMethod.ascending { statusSortName = "By Delivery Status ▼" }
        }
        
        let nameSortingAction = UIAlertAction(title: nameSortName, style: .default) { (_) in
            if self.packageSortingMethod.sortType == .name {
                self.packageSortingMethod.ascending = !self.packageSortingMethod.ascending
            } else {
                self.packageSortingMethod.sortType = .name
                self.packageSortingMethod.ascending = true
            }
        }
        let lastUpdatedSortingAction = UIAlertAction(title: lastUpdatedSortName, style: .default) { (_) in
            if self.packageSortingMethod.sortType == .lastUpdate {
                self.packageSortingMethod.ascending = !self.packageSortingMethod.ascending
            } else {
                self.packageSortingMethod.sortType = .lastUpdate
                self.packageSortingMethod.ascending = true
            }
        }
        let statusSortingAction = UIAlertAction(title: statusSortName, style: .default) { (_) in
            if self.packageSortingMethod.sortType == .status {
                self.packageSortingMethod.ascending = !self.packageSortingMethod.ascending
            } else {
                self.packageSortingMethod.sortType = .status
                self.packageSortingMethod.ascending = true
            }
        }
        
        alertController.addAction(cancelAction)
        alertController.addAction(nameSortingAction)
        alertController.addAction(lastUpdatedSortingAction)
        alertController.addAction(statusSortingAction)
        
        present(alertController, animated: true)
    }
    
    func removeOldDeliveries(numberOfDaysOld days: Int) {
        guard days > 0 else { return }
        
        var numberOfRemovals = 0
        let daysTimeInterval = TimeInterval(-1 * 60 * 60 * 24 * days)
        let cutoffDate = Date(timeIntervalSinceNow: daysTimeInterval)
        
        for (index, package) in packages.enumerated() {
            if let deliveredDate = package.deliveredDate {
                if deliveredDate < cutoffDate {
                    let indexPath = IndexPath(row: index, section: 0)
                    deleteRow(indexPath)
                    numberOfRemovals += 1
                }
            }
        }
        
        guard numberOfRemovals > 0 else { return }
        
        let message = numberOfRemovals == 1 ? "\(numberOfRemovals) package was delivered over \(days) days ago, and was automatically removed." : "\(numberOfRemovals) packages were delivered over \(days) days ago, and were automatically removed."
        
        let alertController = UIAlertController(title: "Old Deliveries Deleted", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func deleteRow(_ indexPath: IndexPath) {
        packages.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .automatic)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "EditPackage" else { return }
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            let destinationViewController = segue.destination as! PackageDetailTableViewController
            destinationViewController.isEditingPackage = true
            destinationViewController.package = packages[selectedIndexPath.row]
        }
    }
    
    @IBAction func unwindToPackageList(segue: UIStoryboardSegue) {
        guard let identifier = segue.identifier else { return }
        let sourceViewController = segue.source as! PackageDetailTableViewController
        
        switch identifier {
        case "SavePackage":
            if let package = sourceViewController.package {
                if let selectedIndexPath = tableView.indexPathForSelectedRow {
                    packages[selectedIndexPath.row] = package
                    tableView.reloadRows(at: [selectedIndexPath], with: .none)
                } else {
                    let newIndexPath = IndexPath(row: packages.count, section: 0)
                    packages.append(package)
                    tableView.insertRows(at: [newIndexPath], with: .automatic)
                }
            }
            
        case "DeletePackage":
            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                deleteRow(selectedIndexPath)
            }
            
        default:
            break
        }
    }
}


extension PackageListTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return packages.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(50)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PackageListCell", for: indexPath) as! PackageListTableViewCell
        
        let package = packages[indexPath.row]
        cell.nameLabel?.text = package.name
        cell.addressLabel?.text = package.address
        cell.status = package.status
        cell.delegate = self
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteRow(indexPath)
        }
    }
}


extension PackageListTableViewController: PackageListTableViewCellDelegate {
    func updateStatus(ofCellAt indexPath: IndexPath, withStatus status: PackageStatus) {
        var package = self.packages[indexPath.row]
        package.status = status
        package.lastUpdated = Date()
        packages[indexPath.row] = package
        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    
    func statusButtonTapped(_ sender: PackageListTableViewCell) {
        if let indexPath = tableView.indexPath(for: sender) {
            
            let alertController = UIAlertController(title: "Set the package status", message: nil, preferredStyle: .actionSheet)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            let packingAciton = UIAlertAction(title: PackageStatus.packing.name, style: .default) { (_) in
                self.updateStatus(ofCellAt: indexPath, withStatus: .packing)
            }
            let pickupAciton = UIAlertAction(title: PackageStatus.awaitingPickup.name, style: .default) { (_) in
                self.updateStatus(ofCellAt: indexPath, withStatus: .awaitingPickup)
            }
            let transitAciton = UIAlertAction(title: PackageStatus.inTransit.name, style: .default) { (_) in
                self.updateStatus(ofCellAt: indexPath, withStatus: .inTransit)
            }
            let deliveredAciton = UIAlertAction(title: PackageStatus.delivered.name, style: .default) { (_) in
                self.updateStatus(ofCellAt: indexPath, withStatus: .delivered)
            }
            
            alertController.addAction(cancelAction)
            if packages[indexPath.row].status != .packing { alertController.addAction(packingAciton) }
            if packages[indexPath.row].status != .awaitingPickup { alertController.addAction(pickupAciton) }
            if packages[indexPath.row].status != .inTransit { alertController.addAction(transitAciton) }
            if packages[indexPath.row].status != .delivered { alertController.addAction(deliveredAciton) }
            present(alertController, animated: true, completion: nil)
        }
    }
}
