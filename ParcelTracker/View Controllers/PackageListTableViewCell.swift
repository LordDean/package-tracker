//
//  PackageListTableViewCell.swift
//  ParcelTracker
//
//  Created by Dean Mollica on 19/6/18.
//  Copyright © 2018 Dean Mollica. All rights reserved.
//

import UIKit

protocol PackageListTableViewCellDelegate: class {
    func statusButtonTapped(_ sender: PackageListTableViewCell)
}

class PackageListTableViewCell: UITableViewCell {
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    var delegate: PackageListTableViewCellDelegate?
    
    var status: PackageStatus? {
        didSet {
            statusButton.setImage(status!.image, for: .normal)
            statusButton.tintColor = status! == .delivered ? UIColor(named: "SystemGreenColor") : super.tintColor
        }
    }
    
    @IBAction func statusButtonTapped() {
        delegate?.statusButtonTapped(self)
    }
    
}
